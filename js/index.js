/**
 * Created by Administrator on 2017/8/5.
 */

/**
 * 调用全屏的方法
 */
$(function(){
    $('#dowebok').fullpage({
        /*设置每一屏的背景*/
        sectionsColor:['#0da5d6', '#2AB561', '#DE8910', '#16BA9D', '#0DA5D6'],
        /* 滚动到某一屏之后调用*/
        afterLoad:function(Link,index){
            // current类加给谁，谁就做动画
            $('.section').removeClass('current');

            //index表示屏幕编号，从1开始; eq()里面的索引值从0开始，所以index-1
            // 让动画延时100毫秒执行
            setTimeout(function () {
                $('.section').eq(index-1).addClass('current');
            },100)

        }
    });
});
